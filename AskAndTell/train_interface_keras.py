#!/usr/bin/env python
# Rui Zhang 4.2020
# rui.zhang@cern.ch

from argparse import ArgumentParser
from os import environ, path

import numpy as np

from train_model_def_keras import objective, readNtuple
from train_util import setupMLflow, RetrieveOpt, RetrievePoints, SaveObjPoints

def main(args):

    library = args.library
    name = args.experiment
    setupMLflow(name)

    n_points = args.n_points
    if args.fix_seed: np.random.seed(args.fix_seed)

    fshare = f"{environ.get('SHAREDIR')}/{name}/{library}"
    opt_name = f'{fshare}/my-optimizer.pkl'
    optimizer = RetrieveOpt(opt_name, library)

    points_name = f'{fshare}/opt_{n_points}points.pkl'
    stored_points = RetrievePoints(points_name)
    
    train_x_S, train_x_B, train_y_S, train_y_B, test_x_S, test_x_B, test_y_S, test_y_B = readNtuple()

    objective_points = []
    for stored_point in stored_points:
        print(stored_point)
        if library == 'skopt':
            values = stored_point
        elif library == 'nevergrad':
            values = stored_point.args
        else:
            assert(False), f'{library} is not supported'
        objective_point = objective(train_x_S, train_x_B, train_y_S, train_y_B, test_x_S, test_x_B, test_y_S, test_y_B, *values)
        objective_points.append(objective_point)
        optimizer.tell(stored_point, objective_point)

    points_name = f'{fshare}/obj_{n_points}points.pkl'
    SaveObjPoints(points_name, stored_points, objective_points)

if __name__ == '__main__':

    '''Get arguments from command line.'''
    parser = ArgumentParser(description="Workforce to train and evaluate objects")
    parser.add_argument('-e', '--experiment', default = 'tmva', type = str, help = 'Name of the your experiment: folder structure and mlflow display')
    parser.add_argument('-p', '--n-points', default = 1, type = int, help = 'Number of points to run')
    parser.add_argument('--fix-seed', default = None, type = int, help = 'Fix random seed for reproduction')

    subparsers = parser.add_subparsers(dest='library', help='HPO library')
    parser_skopt = subparsers.add_parser('skopt', help='https://scikit-optimize.github.io/stable/')
    parser_nevergrad = subparsers.add_parser('nevergrad', help='https://facebookresearch.github.io/nevergrad/')

    main(parser.parse_args())
