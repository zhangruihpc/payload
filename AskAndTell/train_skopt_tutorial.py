#!/usr/bin/env python
# Rui Zhang 3.2020
# rui.zhang@cern.ch

from skopt import Optimizer
from skopt.space import Real, Integer, Categorical
from skopt import load
from argparse import ArgumentParser
import pickle
import numpy as np
from os import environ

def objective(x, noise_level=0.1):
    import numpy as np
    return np.sin(5 * x[0]) * (1 - np.tanh(x[0] ** 2))\
           + np.random.randn() * noise_level

def main(args):
    n_points = args.n_points
    fshare = environ.get('SHAREDIR') + '/' + args.folder_share
    assert(path.exists(fshare)), fshare

    optimizer = load(f'{fshare}/my-optimizer.pkl')
    print('optimizer is', optimizer.Xi, optimizer.yi)

    pkl_name = f'{fshare}/opt_{n_points}points.pkl'
    with open(pkl_name, 'rb') as fp:
        stored_points = pickle.load(fp)
        print('Read opt points from ', pkl_name)
    print(stored_points)

    objective_points = []
    for stored_point in stored_points:
        objective_point = objective(stored_point)
        objective_points.append(objective_point)
        # optimizer.tell(stored_point, objective_point)
    print(objective_points)

    pkl_name = f'{fshare}/obj_{n_points}points.pkl'
    with open(pkl_name, 'wb') as fp:
        pickle.dump([stored_points, objective_points], fp)
        print('Save obj points to ', pkl_name)

if __name__ == '__main__':

    """Get arguments from command line."""
    parser = ArgumentParser(description="scikit-optimize objective evaluation (https://scikit-optimize.github.io/stable/).")
    parser.add_argument('-p', '--n-points', default = 1, type = int, help = 'Number of points to generate')
    parser.add_argument('-e', '--experiment', default = 'skopt', type = str, help = 'Name of the experiment')

    main(parser.parse_args())
