#!/bin/bash
# single run
pip install tensorflow==1.14
pip install keras-drop-block
pip install Keras==2.3.1
python /ATLASMLHPO/payload/CaloImageDNN/run_model.py --exp_dir /ATLASMLHPO/payload/CaloImageDNN/exp_scalars/ --data_path /ATLASMLHPO/payload/CaloImageDNN/dataset/event100.h5 --rm_bad_reco True --zee_only True -g 0

# HPO
# python /ATLASMLHPO/payload/CaloImageDNN/hp_search.py --exp_dir /ATLASMLHPO/payload/CaloImageDNN/exp_scalars/ --data_path /ATLASMLHPO/payload/CaloImageDNN/dataset/event100.h5 --rm_bad_reco True --zee_only True -g 0 --n_train 50 --n_val 20 --n_test 30
