import sys
import json

src = sys.argv[1]
dst = sys.argv[2]

with open(src) as f:
    pars = json.load(f)

new_pars = {}
for par in pars:
    if par in ['cnn_block_depths_1', 'cnn_block_depths_2']:
        if 'cnn' not in new_pars:
            new_pars['cnn'] = {}
        if 'block_depths' not in new_pars['cnn']:
            new_pars['cnn']['block_depths'] = [1, 2, 2, 2, 2]
        if par == 'cnn_block_depths_1':
            new_pars['cnn']['block_depths'][0] = pars[par]
        elif par  == 'cnn_block_depths_2':
            new_pars['cnn']['block_depths'][1] = pars[par]
    else:
        new_pars[par] = pars[par]

with open(dst, 'w') as f:
    json.dump(new_pars, f)