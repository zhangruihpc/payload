# create subset of the input data
import numpy as np
import h5py
import copy
from keras.preprocessing.sequence import pad_sequences
import deepdish as dd

def reduce_data(path, n_points={'train':None, 'val':None, 'test':None}):
    data = {}
    f = h5py.File('test.h5','w')
    with h5py.File(path, 'r') as hf:
        for j in n_points:
            data[j] = {}
            grp = f.create_group(j)
            for i in hf[j].keys():
                f.create_dataset(f'/{j}/{i}', data=hf[j][i][0:100])


def load_atlas_data(path, n_points={'train':None, 'val':None, 'test':None},
                    target_name=None, img_names=None, scalar_names=None,
                    track_names=None, max_tracks=None, sample_weight_name=None,
                    verbose=True):
    """
    Function for loading ATLAS data.

    Args:
    -----
        path : *str*
            Path to the data to be loaded.

        n_points : *dict*
            This dictionary defines the names of the subsets of the data loaded
            (by way of the keys of `n_points`), as well as how many datapoints
            should be loaded for each set (by way of the values of `n_points`).

            Its *keys* should be either `'train'` and `'val'`, or `'train'`,
            `'val'` and `'test'`. This means that while both training and
            validation data are required, test data can be left out. If test
            data is left out, no evaluation will be carried out at the end of
            training. It is important that the naming conventions of `'train'`,
            `'val'` and `'test'` are kept, as this is the only way the
            subsequent handling of the data knows which sets are meant to be
            used for what.

            The *values* of `n_points` dictate how many points of each set will
            be loaded. If `None`, all datapoints in the set corresponding to
            the key will be loaded. If instead a number n (*int* or *float*,
            where the latter will be converted into an *int*), only the first n
            datapoints of that set will be loaded. If a *list* of *int*s, this
            will be interpreted as indices, and only datapoints with these
            indices will be loaded (primarily used in conjunction with data
            generators).

        target_name : *str or list of strs or None*
            Name of target. This should correspond to a dataset in the HDF5 file.
            For energy regression, this is 'p_truth_E', and 'Truth' for particle
            identification.
            If a list, the list must contain two strings, corresponding to two
            datasets in the HDF5 file; the returned target will then be the first
            divided by the second. E.g., for ER, you may want to divide by the
            accordion energy.

        img_names : *list of strs or None*
            List of image names to be loaded. Each image name will have its own
            entry in the returned data dict (acccessed by its name).
            If `None` (default), no images will be loaded and no CNN will be
            constructed.

        scalar_names : *list of strs or None*
            List of scalar variable names (meant to be used in training) to be
            loaded into the `'scalars'` entry of the returned data dict.
            If `None`, no scalar variables will be loaded.

        track_names : *list of strs or None*
            List of scalar variable names (meant to be used in training) to be
            loaded into the `'tracks'` entry of the returned data dict.
            If `None`, no track variables will be loaded.

        max_tracks : *int or None*
            Maximum number of tracks. Sequences less than this will be zero-
            padded, while sequences more than this will be truncated, both in
            the end of the sequence.
            If `None`, the maximum length found in the dataset at hand will be
            used.

        sample_weight_name : *str or None*
            Load sample weights to be used in training. Useful if classes are
            unbalanced or if certain marginal signal/background distributions do
            not follow each other nicely.
            The name should correspond to a dataset in the HDF5 file.

        verbose : *bool*
            Verbose output.

    Returns:
    --------
        data : *dict*
            Dictionary of data. This dictionary (in its first level) will
            contain the same keys as those in n_points. Each of these keys point
            to another dict containing the different datasets (such as images
            and scalars).
            For example, if
            `n_points = {'train':None, 'val':2000, 'test':1e3}` and if
            `scalar_names` is not `None`, then we would access our training set
            scalars (which, like all the other datasets, should be a numpy
            array) like so: `data['train']['scalars']`.
            If we instead want to access the test set images, and `img_names`
            is `['img']`, we do `data['test']['images']['img']`.
            The target (or label in the case of classification) datasets are
            named `'targets'`.
            See the documentation in the README at https://gitlab.com/ffaye/deepcalo 
            for more details.
    """

    if verbose:
        print('Loading data.')

    # Prepare loading only the wanted data points
    # Make copy of n_points to use for loading (changes to n_points will persist
    # outside of this function)
    load_n_points = copy.deepcopy(n_points)

    for set_name in load_n_points:
        # If a list (of specific indices) is not given
        if not hasattr(load_n_points[set_name], '__iter__'):
            # If all data points should be loaded
            if load_n_points[set_name] is None:
                load_n_points[set_name] = slice(None)
            # If only the first int(load_n_points[set_name]) data points should be loaded
            else:
                load_n_points[set_name] = int(load_n_points[set_name])
                n_samples = load_n_points[set_name]
                load_n_points[set_name] = slice(None,load_n_points[set_name])
                if verbose:
                    print(f'Loading only the {n_samples} first data points of the {set_name} set.')
        else:
            load_n_points[set_name] = list(np.sort(load_n_points[set_name]))

    # Load the data
    with h5py.File(path, 'r') as hf:
              
        # Function for loading and concatenating all scalar-likes
        def expand_and_concat(list_to_iter, set_name, n_samples, axis=1):
            return np.concatenate(tuple(np.expand_dims(hf[f'{set_name}/{it}'][n_samples], axis=axis) for it in list_to_iter), axis=axis)

        # Function for loading and concatenating images
        def load_img(img_name, set_name, lrs, samples):
            # Get shape of images
            lr_shape = hf[f'{set_name}/{img_name}_Lr{lrs[0]}'].shape
            
            # Get number of samples to be loaded
            if type(samples)==list:
                lr_shape = (len(samples),) + lr_shape[1:]
            elif type(samples)==slice:
                if n_points[set_name] is not None:
                    lr_shape = (int(n_points[set_name]),) + lr_shape[1:]
              
            # Preallocate
            imgs = np.zeros(lr_shape + (len(lrs),))
              
            for i,lr in enumerate(lrs):
                imgs[:,:,:,i] = hf[f'{set_name}/{img_name}_Lr{lr}'][samples]
            
            return imgs
            # return np.concatenate(tuple(np.expand_dims(hf[f'{set_name}/{img_name}_Lr{lr}'][samples], axis=3) for lr in lrs), axis=3)

        data = {set_name:{'images':{},
                          'scalars':{},
                          'tracks':{},
                          'targets':{},
                          'sample_weights':{}} for set_name in load_n_points}

        # Add targets to data
        if target_name is not None:
            if type(target_name) in [list, tuple]:
                for set_name in load_n_points:
                    data[set_name]['targets'] = hf[f'{set_name}/{target_name[0]}'][load_n_points[set_name]]
                    data[set_name]['targets'] /= hf[f'{set_name}/{target_name[1]}'][load_n_points[set_name]]
            else:
                for set_name in load_n_points:
                    data[set_name]['targets'] = hf[f'{set_name}/{target_name}'][load_n_points[set_name]]

        # Add scalars
        for set_name in load_n_points:
            if scalar_names is not None:
                data[set_name]['scalars'] = expand_and_concat(scalar_names, set_name, load_n_points[set_name])                    

        # Add images
        if img_names is not None:
            # Add cell images to data
            # Different parts of the detector have a different number of layers
            layers_to_include = {img_name:[0,1,2,3] for img_name in ['em_barrel', 'em_endcap', 'lar_endcap']}
            layers_to_include['tile_barrel'] = [1,2,3]
            layers_to_include['tile_gap'] = [1]

            for set_name in load_n_points:
                for img_name in img_names:
                    data[set_name]['images'][img_name] = load_img(img_name, set_name, layers_to_include[img_name], load_n_points[set_name])

        # Add tracks
        if track_names is not None:
            # Add tracks as ndarrays of ndarrays
            for set_name in load_n_points:
                data[set_name]['tracks'] = expand_and_concat(track_names, set_name, load_n_points[set_name])

            # Find maximum number of tracks in any of the datasets
            if max_tracks is None:
                max_tracks = max(max(point[0].shape[0] for point in data[set_name]['tracks']) for set_name in load_n_points)

            # Pad with zeros
            for set_name in load_n_points:
                data[set_name]['tracks'] = np.concatenate(
                    tuple(np.expand_dims(pad_sequences(tracks, maxlen=max_tracks, dtype='float32',
                                                       padding='post', truncating='post', value=0.0).T,
                                         axis=0) for tracks in data[set_name]['tracks']), axis=0)

        # Add sample weights (from reweighting)
        if sample_weight_name is not None:
            for set_name in load_n_points:
                data[set_name]['sample_weights'] = hf[f'{set_name}/{sample_weight_name}'][load_n_points[set_name]]

    if verbose:
        print('Data loaded.')

    return data

if __name__ == '__main__':
    n_points={'train':100, 'val':100, 'test':100}
    #data = reduce_data('/eos/user/l/lehrke/Data/Data/2019-09-26/MC_abseta_1.3_1.6_et_0.0_5000000.0_processes_energy.h5', n_points)

    data = load_atlas_data('/eos/user/l/lehrke/Data/Data/2019-09-26/MC_abseta_1.3_1.6_et_0.0_5000000.0_processes_energy.h5', n_points, img_names=['em_barrel'])
    #data = load_atlas_data('test.h5', n_points)
    print(data)

